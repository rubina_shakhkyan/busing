import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Dimensions
} from 'react-native/Libraries/react-native/react-native-implementation';
import icon from '../assets/images/icons/icon.png';

const Header = () => (
    <View style={styles.header}>
        <Image source={icon} style={styles.logo}/>
    </View>
);
const styles = StyleSheet.create({
    header: {
        width: Dimensions.get('window').width,
        backgroundColor: '#F1D532',
        // alignItems: 'flex-end',
        height: 50,
        position: 'relative'
    },
    logo: {
        height: 40,
        resizeMode: 'contain',
        position: 'absolute',
        right: 40,
        top: 4
    }
});
export default Header;