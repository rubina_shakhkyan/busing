import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Dimensions,
    ImageBackground
} from 'react-native/Libraries/react-native/react-native-implementation';
import Header from '../components/Header';
import StyledText from '../components/StyledText';
import busBg from '../assets/images/icons/busBg.png';
import vanBg from '../assets/images/icons/vanBg.png';
import busIcon from '../assets/images/icons/busIcon.png';

class OrdersScreen extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Header/>
                <View style={styles.orderContainer}  >
                    <ImageBackground style={styles.order} source={busBg} imageStyle={{width: Dimensions.get('window').width*0.9, resizeMode: 'contain'}}>
                        <View>
                            <StyledText>BUS 12LL200 Rostow- Yerevan</StyledText>
                            <StyledText>30-08-2019 13:30 </StyledText>
                            <StyledText>01-09-2019  01:30</StyledText>
                        </View>
                    </ImageBackground>
                    <ImageBackground style={styles.order} source={vanBg} imageStyle={{resizeMode: 'contain'}}>
                        <View>
                            <StyledText>VAN 12LL200 Rostow- Yerevan</StyledText>
                            <StyledText>30-08-2019 13:30 </StyledText>
                            <StyledText>01-09-2019  01:30</StyledText>
                        </View>
                        
                    </ImageBackground>
                </View>
                <View style={{textAlign: 'center', flex: 1,justifyContent: 'flex-end',marginBottom: 36}}>
                    <StyledText style={{textAlign: 'center', marginBottom: 20}}>Current Order</StyledText>
                    <View style={styles.currentOrder}>
                        <Image source={busIcon} style={[styles.image, {resizeMode: 'contain'}]}/>
                        <View>
                            <StyledText>BUS 12LL200 Rostow- Yerevan</StyledText>
                            <StyledText>30-08-2019 13:30 </StyledText>
                            <StyledText>01-09-2019  01:30</StyledText>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginTop: 30,
        textAlign: 'center'
    },
    orderContainer: {
        alignItems: 'center',
        marginTop: 40,
    }, 
    order: {
        backgroundColor: '#f6f6f6',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 5,
        width: Dimensions.get('window').width*0.95,
        padding: 10,
        marginBottom: 15,
    },
    currentOrder: {
        backgroundColor:'#AEEECB',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 5,
        width: Dimensions.get('window').width*0.95,
        padding: 10,
        textAlign: 'center'
    },
    image: {
        width: 60,
        height: 40,
        resizeMode: 'cover',
        marginTop: 15
    }

});
export default OrdersScreen;