import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    TextInput
} from 'react-native/Libraries/react-native/react-native-implementation';
import DatePicker from 'react-native-datepicker';
import Header from '../components/Header';
import Circle from '../components/Circle';
import StyledText from '../components/StyledText';
import Map from '../components/Map';
import bus from '../assets/images/icons/bus.png';
import mnvn from '../assets/images/icons/mnvn.png';
import map from '../assets/images/icons/frame.png';
import compass from '../assets/images/icons/compass.png';
let current_datetime = new Date();
class HomeScreen extends Component {
    state = {
        date: current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear(),
        region: {latitude: 40.179908,  longitude: 44.511295, latitudeDelta: 0.0043, longitudeDelta: 0.0034},
        text: 'Yerevan, Armenia'
    };

    onInteraction = () =>{
    }
    onGeocoding = () => {
    }
    render() {
        return (
            <View style={styles.container}>
                <Header/>
                <StyledText style={{color: '#9A0000', marginBottom: 20, marginTop: 20}}>DESTINATION</StyledText>
                <View style={styles.inputContainer}>
                    <TextInput 
                        onChangeText={(text) => this.setState({text})}
                        value={this.state.text}
                        style={styles.input}
                    />
                    <Image source={compass} style={styles.compass}/>
                </View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Routes')}>
                    <Map style={styles.map}/>
                </TouchableOpacity>
                <StyledText style={{color: '#9A0000', marginBottom: 20, marginTop: 20}}>TRIP DATE</StyledText>
                <DatePicker
                    style={{width: 200, marginBottom: 30}}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="DD-MM-YYYY"
                    minDate={current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                    }}
                    onDateChange={(date) => {this.setState({date: date})}}
                />
                <View style={styles.circleContainer}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Routes', {bus: true})}}>
                        <Circle image={bus} color='#F1D532'  text='BUS' />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Routes', {bus:false})}}>
                        <Circle image={mnvn} color='#F1D532' text='MINIVAN'/>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Orders')}} style={styles.button}>
                    <StyledText>My Orders</StyledText>
                </TouchableOpacity>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginTop: 30,
        textAlign: 'center'
    },
    inputContainer: {
        alignItems: 'center',
        textAlign: 'center'
    },
    circleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '90%',
        alignSelf: 'center',
    }, 
    button: {
        backgroundColor: '#FFEC83',
        borderRadius: 4, 
        borderColor: '#000',
        borderWidth: 0.2,
        paddingLeft: 10, 
        paddingRight: 10,
        paddingTop: 5, 
        paddingBottom: 5,
        marginTop: 20
    },
    // map: {
    //     width: Dimensions.get('window').width*0.8,
    //     height: 200,
    //     resizeMode: 'cover',
    //     marginTop: 30,
    // },
    input: {
        backgroundColor: 'rgba(66, 64, 64, 0.3)',
        borderRadius: 10,
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50,
        width: Dimensions.get('window').width*0.8,
        textAlign: 'center',
        fontFamily: 'quicksand',
        height: 40, 
    }, 
    compass: {
        position: 'absolute',
        right: 0,
        width: 40,
        height:40,
        resizeMode: 'contain'
    }

});
export default HomeScreen;