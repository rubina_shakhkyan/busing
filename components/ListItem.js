import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import StyledText from './StyledText';

export default class ListItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let color = this.props.item.self ? '#FFB700' : '#001751';
        return (
            <View style={{ flexWrap: 'wrap',
                alignItems: 'flex-start',
                flexDirection:'row',
                marginTop: 20
            }}>
                <StyledText style={{color: '#2562FF', fontSize: 22, fontWeight: '500', marginLeft: 20, marginRight: 20, marginTop: 10}}>
                    {this.props.item.key}
                </StyledText>
                <TouchableOpacity>
                    <View style={{flexWrap: 'wrap',
                        alignItems: 'flex-start',
                        flexDirection:'row',
                        borderRadius: 5,
                        shadowColor: 'rgb(205, 205, 205)',
                        shadowOffset: {width: 0, height: 2},
                        shadowOpacity: 0.25,
                        shadowRadius: 5,
                        elevation: 1,
                        paddingTop: 10,
                        paddingBottom: 10,
                        width: 300,
                    }}
                    >
                        <StyledText style={{color: '#001751', fontSize: 18, marginLeft: 20}}>
                            {this.props.item.name}
                        </StyledText>
                        <StyledText style={{color: color, fontSize: 18, textAlign: 'right', position: 'absolute', right: 20, top: 10}}>
                            {this.props.item.points}
                        </StyledText>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
