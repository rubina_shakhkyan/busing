import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    ScrollView
} from 'react-native/Libraries/react-native/react-native-implementation';
import Circle from '../components/Circle';
import Header from '../components/Header';
import StyledText from '../components/StyledText';
import busIcon from '../assets/images/icons/busIcon.png';
import vanIcon from '../assets/images/icons/vanIcon.png';
import busBg from '../assets/images/icons/busBg.png';
import vanBg from '../assets/images/icons/vanBg.png';
import mnvn from '../assets/images/icons/mnvn.png';
import bus from '../assets/images/icons/bus.png';


class RoutesScreen extends Component {
    state={
        bus: this.props.navigation.getParam('bus'),
    }
    switch = (bus) => {
        this.setState({bus});
    }
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container} horizontal={false}>
                <Header/>
                <View style={styles.orderContainer}>
                    <View style={styles.typeContainer}>
                        <TouchableOpacity onPress={()=>this.switch(true)}  >
                            <Circle image={bus} color={this.state.bus ? '#F1D532' : '#E2DFC9'} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.switch(false)} >
                            <Circle image={mnvn} color={!this.state.bus ? '#F1D532' : '#E2DFC9'}  />
                        </TouchableOpacity>
                    </View>
                    { this.state.bus ? (
                        <View>
                            <TouchableOpacity style={styles.order} onPress={()=>{this.props.navigation.navigate('Driver')}}>
                                <Image source={busIcon}  style={styles.image}/>
                                <View>
                                    <StyledText>Rostow-Yerevan</StyledText>
                                    <StyledText>30-08-2019 13:30 </StyledText>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.order} onPress={()=>{this.props.navigation.navigate('Driver')}}>
                                <Image source={busIcon} style={styles.image}/>
                                <View>
                                    <StyledText>Rostow-Yerevan</StyledText>
                                    <StyledText>30-08-2019 13:30 </StyledText>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.order} onPress={()=>{this.props.navigation.navigate('Driver')}}>
                                <Image source={busIcon} style={styles.image}/>
                                <View>
                                    <StyledText>Rostow-Yerevan</StyledText>
                                    <StyledText>30-08-2019 13:30 </StyledText>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ): (
                        <View>
                            <TouchableOpacity style={styles.order} onPress={()=>{this.props.navigation.navigate('Driver')}}>
                                <Image source={vanIcon}  style={styles.image}/>
                                <View>
                                    <StyledText>Rostow-Yerevan</StyledText>
                                    <StyledText>30-08-2019 13:30 </StyledText>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.order} onPress={()=>{this.props.navigation.navigate('Driver')}}>
                                <Image source={vanIcon}  style={styles.image}/>
                                <View>
                                    <StyledText>Rostow-Yerevan</StyledText>
                                    <StyledText>30-08-2019 13:30 </StyledText>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.order} onPress={()=>{this.props.navigation.navigate('Driver')}}>
                                <Image source={vanIcon}  style={styles.image}/>
                                <View>
                                    <StyledText>Rostow-Yerevan</StyledText>
                                    <StyledText>30-08-2019 13:30 </StyledText>
                                </View>
                            </TouchableOpacity>
                        </View>
                    )}                   
                </View>
                <View style={{textAlign: 'center', flex: 1,justifyContent: 'flex-end',marginBottom: 36}} >
                    <Image source={this.state.bus ? busBg : vanBg}/>
                </View>
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        textAlign: 'center',
    },
    orderContainer: {
        alignItems: 'center',
        marginTop: 30,
    }, 
    order: {
        backgroundColor: '#f6f6f6',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 5,
        width: Dimensions.get('window').width*0.8,
        padding: 5,
        marginBottom: 20,
    },
    image: {
        height: 40,
        resizeMode: 'contain',
        marginTop: 15
    },
    typeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Dimensions.get('window').width,
        alignSelf: 'center',
        padding: 20,
    }

});
export default RoutesScreen;