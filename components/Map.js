import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Dimensions} from 'react-native';
import MapView from 'react-native-maps';



export default class Map extends Component {
    state = {
        region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
    }
      
      onRegionChange(region) {
        this.setState({ region });
      }
      

  render() {
    return (
        
    //   <View style={styles.container}>
        <MapView
            style={styles.map}
            region={this.state.region}
            onRegionChange={this.onRegionChange}
            showsUserLocation={true}
            showsCompass={true}
            zoomControlEnabled={true}
            mapType={"standard"}
        />
    //   </View>
    );
  }

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        height: Dimensions.get('window').height*0.4,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        alignSelf: 'stretch', height: 200
    },
});