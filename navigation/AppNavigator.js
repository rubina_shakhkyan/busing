import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import RegisterScreen from '../screens/RegisterScreen';
import OrdersScreen from '../screens/OrdersScreen';
import RoutesScreen from '../screens/RoutesScreen';
import DriverScreen from '../screens/DriverScreen';
import OkScreen from '../screens/OkScreen';


const HomeStack = createStackNavigator(
    {
        Register: {
            screen: RegisterScreen,
            navigationOptions: {
                header: null,
            },
        },
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                header: null,
            },
        },
        Orders: {
            screen: OrdersScreen,
            navigationOptions: {
                header: null,
            },
        },
        Routes: {
            screen: RoutesScreen,
            navigationOptions: {
                header: null,
            },
        },
        Driver: {
            screen: DriverScreen,
            navigationOptions: {
                header: null,
            },
        },
        Ok: {
            screen: OkScreen,
            navigationOptions: {
                header: null,
            },
        },
    },
);

HomeStack.navigationOptions = {
    headerVisible: false
};

HomeStack.path = '';


const tabNavigator = createAppContainer(
    HomeStack,
);

tabNavigator.path = '';

export default tabNavigator;
