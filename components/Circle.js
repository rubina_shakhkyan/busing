import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Dimensions
} from 'react-native/Libraries/react-native/react-native-implementation';
import StyledText from './StyledText';

const Circle = ({image, color, text}) => (
    <View style={[styles.circle, {backgroundColor: color}]}>
        {
            typeof text === 'string' ? (<StyledText style={{fontSize: 12, color: '#5F0000', marginTop: 10}}>{text}</StyledText>):(null)
        }
        <Image source={image} style={styles.image}/>
    </View>
);
const styles = StyleSheet.create({
    circle:{
        width: 106,
        height: 106,
        borderRadius: 100,
        justifyContent: 'center',
        textAlign: 'center',
        alignItems: 'center'
    },
    image: {
        height: 60,
        resizeMode: 'contain'
    }
});
export default Circle;