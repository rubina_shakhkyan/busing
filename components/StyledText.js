import React, {Component} from 'react';
import { Text } from 'react-native';

export default class MonoText extends Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'quicksand', textAlign: 'center' }] } />;
  }
}
