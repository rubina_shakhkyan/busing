import React, {Component} from 'react';
import {ScrollView, View, StyleSheet, Image, TextInput, Dimensions} from 'react-native';
import background from '../assets/images/icons/background.png';
import logo from '../assets/images/icons/logo.png';
import StyledText from '../components/StyledText';
import FullButton from '../components/FullButton';

export default class RegisterScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            requireCode: false,
            code: null,
            validPhone: false,
            editedPhone: false,
        };
    }

    submitPhone = () => {
        console.log(this.state.phone);

        this.setState({requireCode: true});
    };

    submitCode = () => {
        console.log(this.state.code);
        this.props.navigation.navigate('Home');
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container} horizontal={false}>
                <Image source={background} style={styles.image}/>
                <StyledText>
                    {
                        this.state.requireCode ? 'SMS VERIFICATION CODE' : 'YOUR NUMBER'
                    }
                </StyledText>

                {this.state.requireCode ? (
                    <View style={styles.center}>
                        <TextInput
                            style={[styles.input, {marginBottom: 100}]}
                            onChangeText={(code) => this.setState({code})}
                            value={this.state.code}
                            placeholder='123456'
                            keyboardType={'phone-pad'}
                        />
                        <FullButton text="CONFIRM" bg="#FFEC83" color="#D13E34" onClick={this.submitCode}/>
                    </View>
                ) : (
                    <View style={styles.center}>
                        <TextInput
                            style={styles.input}
                            onChangeText={(phone) => this.setState({phone})}
                            value={this.state.phone}
                            placeholder='+1234567890'
                            keyboardType={'phone-pad'}
                        />
                        <Image source={logo} style={styles.logo}/>
                        <FullButton text="NEXT" bg="#FFEC83" color="#D13E34" onClick={this.submitPhone}/>
                    </View>
                )}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    center: {
        textAlign: 'center',
        alignItems:'center',
    },
    image: {
        width: Dimensions.get('window').width,
        height: 300,
        resizeMode: 'contain'
    },
    logo: {
        width: 200,
        height: 200
    },
    input: {
        borderColor: 'white',
        borderWidth: 2,
        height: 60,
        borderRadius: 5,
        marginTop: 20,
        backgroundColor: 'rgba(196, 196, 196, 0.33)',
        fontSize: 24,
        color: '#282828',
        paddingLeft: 20,
        paddingRight: 20,

    }
});
