import React, {Component} from 'react';
import {ScrollView, View, StyleSheet, Image, TextInput, Dimensions, Text} from 'react-native';
import background from '../assets/images/icons/background.png';
import busIcon from '../assets/images/icons/busIcon.png';
import driver from '../assets/images/icons/driver.png';
import fullStar from '../assets/images/icons/fullStar.png';
import emptyStar from '../assets/images/icons/emptyStar.png';
import StyledText from '../components/StyledText';
import FullButton from '../components/FullButton';


export default class OkScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.sircle}>
                <Text>test</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    sircle: {
        position: 'absolute',
        width: 211,
        height: 211,
        left: (Dimensions.get('window').width - 326)/2,
        top: 208,
        backgroundColor: '#42A604',
    },
});
