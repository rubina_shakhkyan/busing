import React, {Component} from 'react';
import {ScrollView, View, StyleSheet, Image, TextInput, Dimensions, Text, TouchableOpacity} from 'react-native';
import background from '../assets/images/icons/background.png';
import busIcon from '../assets/images/icons/busIcon.png';
import driver from '../assets/images/icons/driver.png';
import fullStar from '../assets/images/icons/fullStar.png';
import emptyStar from '../assets/images/icons/emptyStar.png';
import StyledText from '../components/StyledText';
import FullButton from '../components/FullButton';
import Header from '../components/Header';


export default class DriverScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Alert_Visibility: false,
        };
    }

    reserveOrder = () => {
        this.setState({ Alert_Visibility: true });
    };

    closeAlert = () => {
        this.setState({ Alert_Visibility: false });
    };

    render() {
        return (
            <View style={styles.container}>
                <Header/>
                <View style={styles.rating}>
                    <Image source={busIcon} style={styles.bus}/>
                    <View>
                     <View style={styles.stars}>
                        <Image source={fullStar} style={styles.star}/>
                        <Image source={fullStar} style={styles.star}/>
                        <Image source={fullStar} style={styles.star}/>
                        <Image source={fullStar} style={styles.star}/>
                        <Image source={emptyStar} style={styles.star}/>    
                     </View>
                     <StyledText style={{fontSize: 16}}> 88 TT 887</StyledText>
                    </View>
                </View>
                <View style={styles.details}>
                    <View style={styles.bordered}>
                        <StyledText style={{fontSize: 16}}>
                            01 - 09 - 2019
                        </StyledText>
                        <StyledText style={{fontSize: 16}}>
                            Komitas 40
                        </StyledText>
                    </View>
                    <View>
                        <StyledText style={{fontSize: 16}}>
                            Yerevan - Moscow
                        </StyledText>
                    </View>
                </View>
                <View style={styles.driver}>
                    <StyledText style={{fontSize: 16}}>
                        Driver information
                    </StyledText>
                    <Image source={driver} style={styles.image}/>
                    <StyledText style={{fontSize: 16}}>
                        Poxosyan Poxos
                    </StyledText>
                    <StyledText style={{fontSize: 16}}>
                        +37560110110
                    </StyledText>
                </View>    
                <View style={styles.bottom}>
                    <FullButton text="20000 AMD" bg={this.state.Alert_Visibility ? '#AEEECB' : '#FDFFAF'} color="#000" font={14} style={{marginBottom:14}}/>
                    <FullButton text="RESERVE" bg={this.state.Alert_Visibility ? '#ccc' : '#AEEECB'} color="#000" onClick={this.reserveOrder} />
                </View>
                {this.state.Alert_Visibility ? (
                    <View style={styles.alert}>
                        <View style={styles.alertBackdrop}/>
                        <View style={styles.alertContent}>
                            <Text style={styles.alertTitle}>CONFIRM YOUR ORDER</Text>
                            <View style={styles.alertDescription}>
                                <Text style={styles.alertText}>20-10-2019: 10:30</Text>
                                <Text style={styles.alertText}>From: Erevan`</Text>
                                <Text style={styles.alertText}>Komitas Str. 32</Text>
                                <Text style={styles.alertText}>To: Moscow</Text>
                                <Text style={styles.alertText}>Shchelkovskoye Sh., 75</Text>
                            </View>
                            <View style={styles.alertButtons}>
                                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Ok')}} >
                                    <FullButton text="Accept" bg='#88E678' color="#000" font={24}  width={326} />
                                </TouchableOpacity>
                                <FullButton text="Cancel" bg='#00000000' color="#1D1B1B" font={24}  width={326} onClick={this.closeAlert}/>
                            </View>
                        </View>
                    </View>
                ) : (null)}
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Dimensions.get('window').height*0.1,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36
      },
    stars: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Dimensions.get('window').width*0.5,
        alignSelf: 'center',
    },
    star: {
        width: Dimensions.get('window').width*0.08,
        resizeMode:'contain',
        marginRight: Dimensions.get('window').width*0.02
    },
    bus: {
        width: Dimensions.get('window').width*0.45,
        resizeMode: 'contain'
    },
    rating: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Dimensions.get('window').width,
        alignSelf: 'center',
    },
    details: {
        backgroundColor: 'rgba(146, 188, 234, 0.3)',
        borderRadius: 4,
        width: Dimensions.get('window').width*0.9,
        padding: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'center',
    },
    driver: {
        width: Dimensions.get('window').width*0.4,
        alignSelf: 'flex-end',
        marginTop: 30,
        textAlign: 'center',
        alignContent: 'center',
        marginLeft: Dimensions.get('window').width*0.2,

    },
    bordered: {
        borderRightWidth:1,
        borderRightColor: '#282828',
        paddingRight: 20,
    },
    image: {
        width: Dimensions.get('window').width*0.4,
        resizeMode: 'contain',
        alignSelf: 'center'
    },
    alert: {
        position: 'absolute',
        top: 0,
        left: 0,
    },
    alertContent: {
        position:"absolute",
        zIndex: 11,
        width:326,
        height:418,
        left: (Dimensions.get('window').width - 326)/2,
        top: 100,
        backgroundColor:"#FFFFFF",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 15,
        padding: 30,
        paddingTop: 36,
        paddingBottom: 0,
    },
    alertTitle: {
        textAlign: 'center',
        fontFamily: 'quicksand',
        fontSize: 24,
        lineHeight: 30,
        color: '#7B0202',
    },
    alertDescription: {
        marginTop: 40,
    },
    alertText: {
        fontSize: 24,
        lineHeight: 30,
    },
    alertButtons: {
        position: 'absolute',
        bottom: 0,
    },
    alertBackdrop: {
        position: 'absolute',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: '#0000005e',
        top: 0,
        left: 0,
        zIndex: 10,
    },
});
