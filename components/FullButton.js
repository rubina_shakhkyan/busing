import React from 'react';
import {TouchableOpacity, StyleSheet, Text, Dimensions} from 'react-native';


const FullButton = ({bg, color, text, onClick, font, width}) => (
    <TouchableOpacity style={[styles.button, {backgroundColor: bg, width: typeof width !== 'undefined' ? width : Dimensions.get('window').width}]} onPress={onClick}>
        <Text style={{textAlign: 'center', fontSize: typeof font!=='undefined' ? font: 24, color: color}}>{text}</Text>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    button: {
        // width: Dimensions.get('window').width,
        paddingTop: 15,
        paddingBottom: 15,
    }
});

export default  FullButton;